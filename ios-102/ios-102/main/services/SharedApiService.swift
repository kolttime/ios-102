//
//  SharedApiService.swift
//  IOS-102
//
//  Created by Роман Макеев on 22.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import Foundation

class SharedApiService: NSObject {
    static let sharedInstanse : SharedApiService = {SharedApiService()} ()
    private(set) var articleService : ArticleService
    private(set) var commentService : CommentService
    private(set) var photosService : PhotosService
    private override init() {
        self.articleService = ArticleService()
        self.commentService = CommentService()
        self.photosService = PhotosService()
    }
}
