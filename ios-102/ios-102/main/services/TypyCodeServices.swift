//
//  TypyCodeServices.swift
//  IOS-102
//
//  Created by Роман Макеев on 21.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class TypyCodeServices: NSObject {
    typealias RequestJSONComplection = (_ results: JSON?, _ error: Error?) -> Void
    struct APIManager{
        static let sharedManager: SessionManager = {
           let configuration = URLSessionConfiguration.default
            configuration.timeoutIntervalForRequest = 20
            return SessionManager(configuration: configuration)
        }()
    }
    
    internal let host = "https://jsonplaceholder.typicode.com"
    internal func sendRequestWithJSONResponse(
        requestType: HTTPMethod,
        url:String,
        params: [String:AnyObject]?,
        headers: HTTPHeaders?,
        paramsEncoding: ParameterEncoding,
        complection: @escaping RequestJSONComplection
        ){
        var reqHeaders = HTTPHeaders()
        
        if headers != nil {reqHeaders = headers!}
        
        
        APIManager.sharedManager.request(url as URLConvertible, method: requestType, parameters: params, encoding: paramsEncoding, headers: reqHeaders).responseJSON{
            (dataResponse) in
            
           // debugPrint(dataResponse)
            
            switch dataResponse.result {
            case .success(let data) :
                let json = JSON(data)
               // debugPrint(json)
                complection(json,nil)
            case .failure(let error) :
                if let response = dataResponse.response, response.statusCode == 201 {complection(nil,nil)} else if (error as NSError).code != -999 {
                    complection(nil,error)
                }
                
            }
            
        }
    }
    
}

