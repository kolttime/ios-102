//
//  CommentsLoad.swift
//  IOS-102
//
//  Created by Роман Макеев on 26.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import Foundation


protocol commentsDelegate : class {
    func dataLoaded()
    func hasError()
}


class CommentsLoad  {
    var comArr : [[CommentModel]] = []
    weak var delegate : commentsDelegate?
    var dataProviderArticles : AllArticlesDataProvider = AllArticlesDataProvider()
    var dataProviderComments : ArticleDataProvider = ArticleDataProvider()
    
    func loadComments(){
        dataProviderArticles.loadArticles()
        for article in dataProviderArticles.articles {
            dataProviderComments.article = article
            dataProviderComments.loadData()
            comArr.append(dataProviderComments.comments)
        }
        
        delegate?.dataLoaded()
        
    }
    
    
}
