//
//  ArticleDataProvider.swift
//  IOS-102
//
//  Created by Роман Макеев on 23.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import Foundation
import UIKit


protocol ArticleDataProviderDelegate : class {
    func articleDataLoaded()
    func articleDataHasError(error : String)
    func articlePhotoLoaded()
    
}

class ArticleDataProvider: NSObject{
    var article: ArticleModel = ArticleModel()
    var comments : [CommentModel] = []
    var photos : [PhotoModel] = []
    weak var delegate : ArticleDataProviderDelegate?
    weak var delegateCom : ArticleDataProviderDelegate?
    
    func refresh(){
        comments = []
        photos = []
        loadData()
    }
    func loadPhotos(){
        SharedApiService.sharedInstanse.photosService.photos { (photosResponse, errors) in
            if let error = errors {
                self.delegate?.articleDataHasError(error: error)
                return
            } else if let photosArray : [PhotoModel] = photosResponse{
                self.photos = photosArray
                self.delegate?.articlePhotoLoaded()
                return
            }
            self.delegate?.articlePhotoLoaded()
        }
    }
    func loadData(){
        if article.id != nil{
        SharedApiService.sharedInstanse.commentService.comments(postId: article.id!) { (commentResponse, errors) in
            if let error = errors {
                 self.delegate?.articleDataHasError(error: error)
                 self.delegateCom?.articleDataHasError(error: error)
                return
            } else if let commentsArray: [CommentModel] = commentResponse{
                self.comments = commentsArray
                SharedApiService.sharedInstanse.photosService.photos { (photosResponse, errors) in
                    if let error = errors {
                        self.delegate?.articleDataHasError(error: error)
                        return
                    } else if let photosArray : [PhotoModel] = photosResponse{
                        self.photos = photosArray
                        var i = 0
                        for comment in self.comments {
                            comment.url = self.photos[i].thumbnailUrl
                            i += 1
                        }
                        
                        self.delegate?.articlePhotoLoaded()
                        return
                    }
                    self.delegate?.articlePhotoLoaded()
                }
                self.delegate?.articleDataLoaded()
                
                
            }
            self.delegate?.articleDataLoaded()
        }
            
        }
        
        
        
    }
}
