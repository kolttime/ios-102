//
//  AllArticlesDataProvider.swift
//  IOS-102
//
//  Created by Роман Макеев on 22.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import Foundation
import UIKit

protocol AllArticlesDataProviderDelegate : class {
    func articlesDataLoaded()
    func articlesDataHasError(error : String)
    func commentsLoaded()
    
}

class AllArticlesDataProvider: NSObject{
    var dataProviderCom = ArticleDataProvider()
    var articles: [ArticleModel] = []
    var comments : [[CommentModel]] = [[]]
    
    weak var delegate : AllArticlesDataProviderDelegate?
    func refresh(){
        articles = []
        loadArticles()
    }
    func loadComments(){
       
        for article in articles{
            
            SharedApiService.sharedInstanse.commentService.comments(postId: article.id!) { (commentResponse, errors) in
                if let error = errors {
                    self.delegate?.articlesDataHasError(error: error)
                    return
                } else if let commentsArray: [CommentModel] = commentResponse{
                    self.comments.append(commentsArray)
                    article.comCount = commentsArray.count
                    self.delegate?.commentsLoaded()
                    return
                }
            }
        }
        self.delegate?.commentsLoaded()
    
    }
    func loadArticles(){
        SharedApiService.sharedInstanse.articleService.posts(limit: 100, skip: 0) { (articlesResponse, errors) in
            if let error = errors{
                self.delegate?.articlesDataHasError(error: error)
                return
            } else if let articles: [ArticleModel] = articlesResponse{
                self.articles.append(contentsOf: articles)
                //self.loadComments()
                self.delegate?.articlesDataLoaded()
                
                
        }
        
        
            
        }
        
    }
}
