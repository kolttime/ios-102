//
//  articleRoute.swift
//  IOS-102
//
//  Created by Роман Макеев on 21.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import Foundation
import UIKit

class articleRoute {
    static func showArticle(article: ArticleModel, fromVc: UIViewController){
        let toVc = UIStoryboard(name: "Article", bundle: nil).instantiateViewController(withIdentifier: ArticleTableViewController.nibName) as! ArticleTableViewController
        toVc.dataProvider.article = article 
        fromVc.navigationController?.pushViewController(toVc, animated: true)
    }
}
