//
//  ArticleService.swift
//  IOS-102
//
//  Created by Роман Макеев on 22.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import Alamofire
import ObjectMapper
import SwiftyJSON

typealias ArticlePostCompetion = (_ articles : [ArticleModel]?, _ error : String?) -> Void
class ArticleService: TypyCodeServices {
    func posts(limit: Int, skip: Int, competion : @escaping ArticlePostCompetion){
        var params : [String : AnyObject] = [:]
        params["limit"] = limit as AnyObject
        params["skip"] = skip as AnyObject
        let url = host + "/posts"
        self.sendRequestWithJSONResponse(
            requestType: HTTPMethod.get,
            url: url,
            params: params,
            headers: nil,
            paramsEncoding: URLEncoding.default){
           (responseData, error) in
                if error != nil {
                    competion(nil, error?.localizedDescription)
                    return
                } else if let articles = responseData?.arrayObject {
                    let ArticleModels = Mapper<ArticleModel>().mapArray(JSONObject: articles)
                    competion(ArticleModels, nil)
                    return
                }
                competion(nil, "Ошибка")
        }
        
            
        
    }
}
