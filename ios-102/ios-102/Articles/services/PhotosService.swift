

import Foundation
import ObjectMapper
import Alamofire
import SwiftyJSON


typealias PhotosCompetion = (_ comments: [PhotoModel]?, _ error : String? ) -> Void
class PhotosService: TypyCodeServices {
    func photos(competion: @escaping PhotosCompetion){
        let url = host + "/photos"
        
        self.sendRequestWithJSONResponse(requestType: HTTPMethod.get,
                                         url: url,
                                         params: nil,
                                         headers: nil,
                                         paramsEncoding: URLEncoding.default) {
                                            (responseData, error) in
                                            if error != nil {
                                                competion(nil, error?.localizedDescription)
                                                return
                                            } else if let photos = responseData?.arrayObject{
                                                let photosModel = Mapper<PhotoModel>().mapArray(JSONObject: photos)
                                                competion(photosModel, nil)
                                                return
                                            }
                                            competion(nil, "Ошибка")
        }
    }
}

