//
//  CommentService.swift
//  IOS-102
//
//  Created by Роман Макеев on 23.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import Foundation
import ObjectMapper
import Alamofire
import SwiftyJSON


typealias ComentsCompetion = (_ comments: [CommentModel]?, _ error : String? ) -> Void
class CommentService: TypyCodeServices {
    func comments(postId: Int, competion: @escaping ComentsCompetion){
        let url = host + "/comments"
        
        var params: [String : AnyObject] = [:]
        params["postId"] = postId as AnyObject
        
        self.sendRequestWithJSONResponse(requestType: HTTPMethod.get,
                                         url: url,
                                         params: params,
                                         headers: nil,
                                         paramsEncoding: URLEncoding.default) {
                                            (responseData, error) in
                                            if error != nil {
                                                competion(nil, error?.localizedDescription)
                                                return
                                            } else if let comments = responseData?.arrayObject{
                                                let commentsModel = Mapper<CommentModel>().mapArray(JSONObject: comments)
                                                competion(commentsModel, nil)
                                                return
                                            }
                                            competion(nil, "Ошибка")
        }
    }
}
