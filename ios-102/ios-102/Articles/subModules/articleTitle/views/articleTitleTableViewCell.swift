//
//  articleTitleTableViewCell.swift
//  IOS-102
//
//  Created by Роман Макеев on 21.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import UIKit

class articleTitleTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    static let nibName = "articleTitleTableViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func customize(model: ArticleModel){
        titleLabel.text = model.title
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
