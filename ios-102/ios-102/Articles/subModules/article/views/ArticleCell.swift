//
//  ArticleCell.swift
//  IOS-102
//
//  Created by Роман Макеев on 20.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import UIKit

class ArticleCell: UITableViewCell {
    
    
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    static let nibName = "ArticleCell"
    
   
    @IBOutlet weak var comCount: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodylabel: UILabel!
    
    func customize(article: ArticleModel, count: Int){
        
        titleLabel.text = article.title
        bodylabel.text = article.body
        comCount.text = String(count)
        if !activityIndicator.isAnimating {
            comCount.isHidden = false
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        
        activityIndicator.startAnimating()
        // Initialization code
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowRadius = 10
        self.layer.shadowOpacity = 10
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
}
