//
//  articleCommentTableViewCell.swift
//  IOS-102
//
//  Created by Роман Макеев on 21.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import UIKit
import Kingfisher

class articleCommentTableViewCell: UITableViewCell {
    @IBOutlet weak var photoImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    static let nibName = "articleCommentTableViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
        let separator = UIView(frame: CGRect(x: 19, y: 0, width: bounds.size.width + 190 , height: 1))
        separator.backgroundColor = UIColor(red: 0.85, green: 0.85, blue: 0.85, alpha: 0.5)
        
        contentView.addSubview(separator)
        photoImage.layer.borderWidth = 1
        photoImage.layer.masksToBounds = false
        photoImage.layer.borderColor = UIColor.black.cgColor
        photoImage.layer.cornerRadius = photoImage.frame.height/2
        photoImage.clipsToBounds = true
        // Initialization code
    }
    func customize(commentModel : CommentModel){
        if commentModel.url != nil {
        let resourse = ImageResource(downloadURL: URL(string: commentModel.url!)!, cacheKey: String(commentModel.id!))
        photoImage.kf.setImage(with: resourse)
        }
        nameLabel.text = commentModel.name
        bodyLabel.text = commentModel.body
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
