//
//  AllAArticlesViewController.swift
//  IOS-102
//
//  Created by Роман Макеев on 20.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import UIKit

class AllAArticlesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, AllArticlesDataProviderDelegate {
    
    
   
    
    
    @IBOutlet weak var TableView: UITableView!
    
    var dataProvider : AllArticlesDataProvider = AllArticlesDataProvider()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil )
        dataProvider.delegate = self
        
        customizeTableCell()
        dataProvider.loadArticles()
        dataProvider.loadComments()
        TableView.estimatedRowHeight = 50.0
        TableView.rowHeight = UITableViewAutomaticDimension
        
        
        
        
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func customizeTableCell(){
        TableView.register(UINib(nibName: ArticleCell.nibName, bundle: nil), forCellReuseIdentifier: ArticleCell.nibName)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataProvider.articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ArticleCell.nibName) as! ArticleCell
        let count = dataProvider.articles[indexPath.row].comCount
        cell.customize(article: dataProvider.articles[indexPath.row], count: count)
        cell.layer.zPosition = CGFloat(dataProvider.articles.count - indexPath.row.hashValue)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        articleRoute.showArticle(article: dataProvider.articles[indexPath.row], fromVc: self)
    }
    
    func articlesDataLoaded() {
        TableView.reloadData()
        dataProvider.loadComments()
    }
    
    func articlesDataHasError(error: String) {
        debugPrint(error)
    }
    func commentsLoaded(){
        TableView.reloadData()
        debugPrint("Успех!")
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
