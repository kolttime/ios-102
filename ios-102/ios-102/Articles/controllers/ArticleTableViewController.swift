//
//  ArticleTableViewController.swift
//  IOS-102
//
//  Created by Роман Макеев on 21.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import UIKit

class ArticleTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,  ArticleDataProviderDelegate {
    
    
    
    
    
    @IBOutlet weak var TableView: UITableView!
    static let nibName = "ArticleTableViewController"
    public var dataProvider : ArticleDataProvider = ArticleDataProvider()
    override func viewDidLoad() {
        
        super.viewDidLoad()
        dataProvider.delegate = self
        customizeTableCell()
        dataProvider.loadData()
        
        TableView.estimatedRowHeight = 50.0
        TableView.rowHeight = UITableViewAutomaticDimension
        TableView.separatorStyle = .none

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    // MARK: - Table view data source
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {return 1}
        else if section == 1 {return 1}
        else {return dataProvider.comments.count}
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section != 2 {return 2.5} else {return 0}
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: articleTitleTableViewCell.nibName) as! articleTitleTableViewCell
            cell.customize(model: dataProvider.article)
            
            return cell
        }
        else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: articleContentTableViewCell.nibName) as! articleContentTableViewCell
            cell.customize(model: dataProvider.article)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: articleCommentTableViewCell.nibName) as! articleCommentTableViewCell
            cell.customize(commentModel: dataProvider.comments[indexPath.row])
            return cell
        }
    }
    
    
    func customizeTableCell(){
        TableView.register(UINib(nibName: articleTitleTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: articleTitleTableViewCell.nibName)
        TableView.register(UINib(nibName: articleContentTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: articleContentTableViewCell.nibName)
        TableView.register(UINib(nibName: articleCommentTableViewCell.nibName, bundle: nil), forCellReuseIdentifier: articleCommentTableViewCell.nibName)
    }
     
    func articleDataLoaded() {
        TableView.reloadData()
        
    }
    
    func articleDataHasError(error: String) {
        // Реализация ошибки
        debugPrint(error)
    }
    
    func articlePhotoLoaded() {
        TableView.reloadData()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
