//
//  PhotoModel.swift
//  IOS-102
//
//  Created by Роман Макеев on 27.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import Foundation
import ObjectMapper

class PhotoModel : NSObject, Mappable{
    
    var albumId : Int?
    var id : Int?
    var title : String?
    var url : String?
    var thumbnailUrl : String?
    
    override init(){
        super.init()
    }
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    func mapping(map: Map) {
        albumId             <- map["albumId"]
        id                  <- map["id"]
        title               <- map["title"]
        url                 <- map["url"]
        thumbnailUrl        <- map["thumbnailUrl"]
    }
}
