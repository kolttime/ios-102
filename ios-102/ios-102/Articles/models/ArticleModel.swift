//
//  ArticleModel.swift
//  IOS-102
//
//  Created by Роман Макеев on 21.06.2018.
//  Copyright © 2018 Роман Макеев. All rights reserved.
//

import Foundation
import ObjectMapper

class ArticleModel: NSObject, Mappable {
    
    var id : Int?
    var userId: Int?
    var title: String?
    var body: String?
    var comCount: Int = 0
    
    override init(){
        super.init()
    }
    required init?(map: Map) {
        super.init()
        self.mapping(map: map)
    }
    func mapping(map: Map) {
        id         <- map["id"]
        userId     <- map["userId"]
        title      <- map["title"]
        body       <- map["body"]
    }
}
